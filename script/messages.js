// Stack class 
class Stack { 
    _size = 0;
    // Array is used to implement stack 
    constructor() 
    { 
        this.items = []; 
    } 
  
    // Functions to be implemented 
    // push function 
    get getSize(){
        return this.items.length;
    }
    push(element) 
    { 
        // push element into the items 
        this.items.push(element);
        this._size++;
    }     
    // pop function 
    pop() 
    { 
        // return top most element in the stack 
        // and removes it from the stack 
        // Underflow if stack is empty 
        
        if (this.items.length == 0) 
            return "Underflow"; 
        this._size--;    
        return this.items.pop(); 
    } 
    // peek function 
    peek() 
    { 
        // return the top most element from the stack 
        // but does'nt delete it. 
        return this.items[this.items.length - 1]; 
    } 
    // isEmpty function 
    isEmpty() 
    { 
        // return true if stack is empty 
        return this.items.length == 0; 
    } 
    // printStack function 
    printStack() 
    { 
        var str = ""; 
        for (var i = 0; i < this.items.length; i++) 
            str += this.items[i] + " "; 
        return str; 
    } 
}

class Message {
    constructor(data) {
        this._value = data;
        this._para = [];
        this.isMessage = false;
    }
}


function Parser(str){
    // return tree parsing
    var mess = new Message(str);
    let tmp = "";
    let n = str.length;
    let stack = new Stack;
    let nComma = 0;
    for (let i = 0; i < n; i++){
        if (str[i] === '('){
            if (nComma === 0 && mess.isMessage === false) {
                mess.isMessage = true;
                stack.push(str[i]);
                tmp = "";
                continue;
            }
            
            else{stack.push(str[i]);}
        }
        else if (str[i] === ')'){
            stack.pop();
            // case in the last message
            if (stack.isEmpty() && i === n - 1){
                if (tmp !== ""){
                    var tmpMess = new Message(tmp.trim());
                    tmp = "";
                    mess._para.push(tmpMess);
                    nComma++;
                    break;
                }
            }
            // case if mess is a fucntion
            else if (stack._size === 1 && mess.isMessage === true){
                if (tmp !== ""){
                    tmp = tmp + str[i];
                    var tmpMess = new Message(tmp.trim());
                    tmp = "";
                    mess._para.push(tmpMess);
                    nComma++;
                    continue;
                }
            }
        }
        else if (str[i] === ','){
            if (stack.isEmpty() || (stack._size === 1 && mess.isMessage === true)){
                if (tmp !== ""){
                    var tmpMess = new Message(tmp.trim());
                    tmp = "";
                    mess._para.push(tmpMess);
                    nComma++;
                    continue;
                }
                else{
                    if (tmp.length === 0) continue;
                }
            }
        }
        tmp = tmp + str[i];
        // case last parameters and no ')'
        if (i === n-1 && mess._para.length > 0 && mess.isMessage === false){
            if (tmp !== ""){
                var tmpMess = new Message(tmp.trim());
                tmp = "";
                mess._para.push(tmpMess);
                nComma++;
                break;
            }
        }
    }
    if (nComma !== mess._para.length){
        console.log("comma: " + nComma);
        console.log("parameters: " + mess._para.length);
    }
    if (mess._para.length > 0) {
        for (let i = 0; i < mess._para.length; i++){
            mess._para[i] = Parser(mess._para[i]._value);
            //console.log(mess._para[i]);
        }
    }
    return mess;

}

function Compare(mess, comparison){
    let tmp = mess;
    for (let i = 0; i < comparison.length; ++i){
        tmp = tmp._para[comparison[i]];
    }
    return tmp._value;
}
/*
var x = "(0,approaching,0,oo)";
// var x = "(123,123)";
var comp = [];
comp.push(1);
//comp.push(1);
temp = Parser(x);
console.log(temp);
console.log(Compare(temp,comp));
*/
/*
if (temp._para.length > 0) {
    for (let i = 0; i < temp._para.length; i++){
        temp._para[i] = Parser(temp._para[i]._value);
        console.log(temp._para[i]);
    }
}
*/
// console.log(temp);
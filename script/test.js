// run "Run" funciton button
function draw() {

    var data = loadData();
    allData = data;
    initTime();
    svg = d3.select("#svgPicture").select("svg");
    //alert(svg);
    elementDisplayState = d3.select("#diplayStateText");
    //alert(elementDisplayState);
    MakeTransition(data, speed);
}

// run "Stop" function button
function stopSVG() {
    svg.selectAll("text").transition().duration(0).delay(0);

    d3.select("#diplayStateText").transition().duration(0).delay(0);

    clearTimeout(running);

    var index = d3.select("#diplayStateText").attr("stepIndex");

    if (!checkUndefined(index)) {
        if (Number(index) > parseInt(index)) stepIndex = parseInt(index) + 1;
        else stepIndex = parseInt(index);

        if (stepIndex >= allData.length) {
            stepLoopIndex = stepIndex - allData.length;
        }
    }


}

// run "Back step" function button
function backStep() {
    var loopLength = loopStates.length;
    svg = d3.select("#svgPicture").select("svg");

    if (stepIndex == 0 || (checkUndefined(allData.length))) {
        allData = loadData();
        stepIndex = lengthData - 1;
        if (loopStates.length > 0) {
            stepIndex = stepIndex + loopLength;
            stepLoopIndex = loopLength - 1;

        }
        elementDisplayState = d3.select("#diplayStateText");

    }
    else {
        stepIndex = stepIndex - 1;
        if (stepIndex > lengthData) stepLoopIndex = stepIndex - lengthData;
    }

    drawStep();
    stepIndex = stepIndex - 1;


}

// run "Draw step" function button
function drawStep() {
    svg = d3.select("#svgPicture").select("svg");
    if (stepIndex == 0 || (checkUndefined(allData.length))) {
        allData = loadData();
        //console.log("type of allData: " + typeof allData);
        //console.log(allData);
        elementDisplayState = d3.select("#diplayStateText");
        lengthData = allData.length;
    }
    initTime();
    var state;;
    var next_state;
    // run while current state is not over.
    if (stepIndex < lengthData) {
        if (stepIndex == 0) {
            state = allData[stepIndex];
            next_state = state;
            var index = 0;
            StateGraph(state, next_state, index);
        }
        else {
            state = allData[stepIndex - 1];
            next_state = allData[stepIndex];
            StateGraph(state, next_state, stepIndex);
        }
        stepIndex = stepIndex + 1;
    }
    else {
        // for looping
        if (lengthLoopStates == 0) {
            stepIndex = 0;
            clearTimeout(running);
            // stopSVG();
        }
        else {
            if (stepLoopIndex == 0) {
                if (loop) state = loopStates[lengthLoopStates - 1];
                else state = allData[stepIndex - 1];
                next_state = loopStates[0];
                StateGraph(state, next_state, stepIndex);
                stepLoopIndex = stepLoopIndex + 1;
                stepIndex = stepIndex + 1;
            }
            else {
                if (stepLoopIndex < loopStates.length) {
                    state = loopStates[stepLoopIndex - 1];
                    next_state = loopStates[stepLoopIndex];

                    StateGraph(state, next_state, stepIndex);

                    stepLoopIndex = stepLoopIndex + 1;
                    stepIndex = stepIndex + 1;
                }
                else {
                    stepIndex = stepIndex - stepLoopIndex;
                    stepLoopIndex = 0;
                    loop = true;
                }
            }
        }
    }
}

function initTime() {
    speed = $('#slider').slider('value');
    wait = speed / 2;
    time = 0;

    //init groups: disable

}

/*
parameter
    + data: full data content (key, text display)
    + speed: value 
aim
    + using drawStep() with speed
*/
function MakeTransition(data, speed) {

    var length = data.length;


    running = setInterval(function () { drawStep(); }, time + wait + speed);


}


function StateGraph(state, next_state, index) {

    $(".groups").removeClass("show");
    $(".groups").addClass("hidden");
    //d3.selectAll(".groups").attr("class","groups hidden");

    var numKeys = allKeys.length;
    var k, s1, s2, element;

    var displayState = "";
    var iterMessContainerName = 0; // iter for messDisplay.messContainerName

    for (var i = 0; i < numKeys; i++) {
        var key = allKeys[i];
        s1 = state[key].trim();
        s2 = next_state[key].trim();
        displayState = displayState + " (" + key + ": " + s2 + " )";
        element = svg.selectAll("[id='" + svgIDs[i] + "']");
        var tmpValue = texDisplays[key];
        
        if (checkUndefined(element) == false && element[0].length > 0) {
            if (index == 0) {
                if (!checkUndefined(tmpValue)) {
                    var option = tmpValue[0];
                    if (option == "VER" || option == "VER-REV") {
                        // NormalizeTextDisplay(key,s1, "black", speed, time);
                        setTimeout(NormalizeTextDisplay(key, s1, "black", speed, time), time);
                        time = time + wait;

                    }
                    if (option == "REV" || option == "NONE") {
                        element.transition().text(NormalizeText(key, s1)).attr("fill", "black").duration(speed).delay(time);
                    }
                }
                else {
                    element.transition().text(s1).attr("fill", "black").duration(speed).delay(time);
                }

            }
            var color;
            if (s1 != s2) color = "red";
            else color = "black";

            if (!checkUndefined(tmpValue)) {
                var option = tmpValue[0];
                if (option == "VER" || option == "VER-REV") {
                    setTimeout(NormalizeDisplay(key, s2, color, speed, time + wait), time + wait);
                    time = time + wait;
                }
                if (option == "REV" || option == "NONE") {
                    element.transition().text(NormalizeText(key, s2)).attr("fill", color).duration(speed).delay(time + wait);
                }
            }
            else {
                element.transition().text(s2).attr("fill", color).duration(speed).delay(time + wait);
            }
        }
        else {
            // process with conditionMess
            //if (svgIDs[i] === messDisplay.messContainerName[iterMessContainerName]){
                /*
                iterMessContainerName++;
                let n = messDisplay.messContainter[svgIDs[i]].length;
                for (let j = 0; j < n; ++j){
                    let tmp = messDisplay.messContainter[svgIDs[i]][j]; // nw_msg#0
                    let value = getMessValue(s1,svgIDs[i],tmp); // get value from position nw_msg#0 of key svgIDs[i] in s1
                    // case: key-value
                    element = svg.selectAll("[id='" + tmp + "']");
                    if (checkUndefined(element) == false && element[0].length > 0){
                        element.transition().text(value).attr("fill", "black").duration(speed).delay(time);
                    }
                    // case: key-value-value
                    element = svg.selectAll("[id='" + tmp + "_" + value.replace("'", "&#39;") + "']");
                    if (!checkUndefined(element) && element[0].length > 0) {
                        element.attr("class", "groups show");
                    }
                }
                */
            //}

            // process with textDisplay
            if (!(tmpValue === undefined || tmpValue.length == 0)){
                var tmp = Parser(s2);
                for (let j = 0; j < tmpValue.length; ++j){ // tmpValue[j] = array
                    for (let jj = 0; jj < tmpValue[j].length; ++jj){
                        element = svg.selectAll("[id='" + tmpValue[j][jj] + "']");
                        var getValueCondition = function(sample){  
                            var arr = [];
                            for (let k = key.length; k < sample.length; ++k){
                                if (sample[k] != "#"){
                                    arr.push(parseInt(sample[k]));
                                }
                            }
                            return arr;
                        }
                        // console.log("condition: " + getValueCondition(tmpValue[j][jj]));
                        var tmpV = Compare(tmp,getValueCondition(tmpValue[j][jj]));
                        if (!checkUndefined(element) && element[0].length > 0){
                            element.transition().text(tmpV).attr("fill", "black").duration(speed).delay(time + wait);
                        }
                        else{
                            element = svg.selectAll("[id='" + tmpValue[j][jj] + "_" + tmpV.replace("'", "&#39;") + "']");
                            if (!checkUndefined(element) && element[0].length > 0) {
                                element.attr("class", "groups show");
                                // alert("checked: " + "[id='" + svgIDs[i] + "_" + s2.replace("'", "&#39;") + "']");
                            }
                        }
                    }
                }
                
            }
            // process with nw-all
            element = svg.selectAll("[id='" + svgIDs[i] + "-" + "all" + "']");

            if (!checkUndefined(element) && element[0].length > 0) {
                // do with bbox
                let rect = svg.selectAll("[id='" + "__" + svgIDs[i] + "-" + "all" + "']");
                if (!checkUndefined(rect) && rect[0].length > 0) {

                    let rectWidth = rect.node().getBBox().width;
                    let rectHeight = rect.node().getBBox().height;

                    // caculate distance to display 
                    let endPoint = { "x": parseFloat(element.attr("x")), "y": (parseFloat(element.attr("y")) + rectHeight) };
                    //console.log("endPoint of " + svgIDs[i] + ": " + endPoint.x + ", " + endPoint.y);
                    let firstPoint = { "x": parseFloat(element.attr("x")), "y": parseFloat(element.attr("y")) };
                    //console.log("firstPoint of " + svgIDs[i] + ": " + firstPoint.x + ", " + firstPoint.y);
                    let distance = Math.sqrt((endPoint.x - firstPoint.x) * (endPoint.x - firstPoint.x) + (endPoint.y - firstPoint.y) * (endPoint.y - firstPoint.y));
                    let maxLine = Math.floor(distance / 28);
                    //console.log("maximum of lines: " + Math.floor(distance / 28 + 1));

                    let curState = getStatesInString(s2);

                    // delete teMessages 
                    //console.log(svg.selectAll("._dot" + svgIDs[i]).remove());
                    svg.selectAll("._dot" + svgIDs[i]).remove();
                    svg.selectAll("._new" + svgIDs[i]).remove();

                    // variables for display
                    let cacheLine = curState[0];
                    let heightline = 28;
                    let remainMess = "";
                    // in case maximum of line = 1 or first mess isn't enough space for width of rectangle
                    if (element.text(cacheLine).style("visibility", "hidden").node().getBBox().width > rectWidth || maxLine == 1) { // display all of message with ...
                        element.text("");
                        for (let iter = 1; iter < curState.length; ++iter) {
                            cacheLine = cacheLine + " ; " + curState[iter];
                        }
                        let teMessages = svg.append("text")
                            .attr("class", "_dot" + svgIDs[i])
                            .attr("x", element.attr("x"))
                            .attr("y", element.attr("y"))
                            //.attr("dy", heightline.toString())
                            .on("click", function mouseClick() { alert(cacheLine) });
                        teMessages.transition().text("...").attr("fill", "black").duration(speed).delay(time + wait);
                    }

                    else {
                        // in case only 1 message in nw
                        if (curState.length == 1) {
                            element.transition().text(s2).style("visibility","visible").attr("fill", "black").duration(speed).delay(time + wait);
                        }
                        else {
                            let newLine = cacheLine;
                            let nLine = 1;
                            let line1 = "";
                            for (let iter = 1; iter < curState.length; ++iter) {
                                cacheLine = cacheLine + " ; " + curState[iter];
                                if (element.text(cacheLine).style("visibility", "hidden").node().getBBox().width > rectWidth) {
                                    if (nLine == 1) { // in case, the first line only display in element variable
                                        line1 = newLine;
                                    }
                                    else {
                                        if (nLine == maxLine) { // the last line is the line which contain all of the remain messages
                                            //element.transition().text(line1).attr("fill","black").duration(speed).delay(time+wait);
                                            remainMess = cacheLine;
                                            for (let iter1 = iter + 1; iter1 < curState.length; ++iter1) {
                                                remainMess = remainMess + " ; " + curState[iter1];
                                            }
                                            break;
                                        }
                                        else {
                                            let teMessages = svg.append("text")
                                                .attr("class", "_new" + svgIDs[i])
                                                .attr("x", element.attr("x"))
                                                .attr("y", element.attr("y"))
                                                .attr("dy", heightline.toString())
                                            teMessages.transition().text(newLine).attr("fill", "black").duration(speed).delay(time + wait);
                                            heightline += 28;
                                        }
                                    }
                                    cacheLine = curState[iter];
                                    newLine = curState[iter];
                                    nLine++;
                                }
                                else {
                                    newLine = cacheLine;
                                    flagFull = false;
                                }
                            }
                            if (remainMess != "") {
                                let teMessages = svg.append("text")
                                    .attr("class", "_dot" + svgIDs[i])
                                    .attr("x", element.attr("x"))
                                    .attr("y", element.attr("y"))
                                    .attr("dy", heightline.toString())
                                    .on("click", function mouseClick() { alert(remainMess) });
                                teMessages.transition().text("...").attr("fill", "black").duration(speed).delay(time + wait);
                            }

                            else {
                                if (nLine != 1) {
                                    let teMessages = svg.append("text")
                                        .attr("class", "_new" + svgIDs[i])
                                        .attr("x", element.attr("x"))
                                        .attr("y", element.attr("y"))
                                        .attr("dy", heightline.toString())
                                    teMessages.transition().text(newLine).attr("fill", "black").duration(speed).delay(time + wait);
                                }
                                else if (nLine == 1) {
                                    element.transition().text(newLine).style("visibility","visible").attr("fill", "black").duration(speed).delay(time + wait);
                                }
                            }
                            if (line1 != "") {
                                element.transition().text(line1).style("visibility","visible").attr("fill", "black").duration(speed).delay(time + wait);
                            }
                        }
                    }
                }



            }

            // process with nw-receive
            element = svg.selectAll("[id='" + svgIDs[i] + "-" + "receive" + "']");

            if (!checkUndefined(element) && element[0].length > 0) {
                element.transition().text("").attr("fill", "black").duration(speed).delay(time + wait);
                var color = "red";
                if (s1 != s2) color = "red";
                else color = "black";


                var lastState = getStatesInString(s1);
                var curState = getStatesInString(s2);
                if (s1 != s2) {
                    curState = getStateChange(lastState, curState);

                    if (curState[0] != "void")
                        element.transition().text(curState.join(" ; ")).attr("fill", color).duration(speed).delay(time + wait);

                }
            }

            // process with nw-send
            element = svg.selectAll("[id='" + svgIDs[i] + "-" + "send" + "']");

            if (!checkUndefined(element) && element[0].length > 0) {
                element.transition().text("").attr("fill", "black").duration(speed).delay(time + wait);
                var color = "red";
                if (s1 != s2) color = "red";
                else color = "black";


                var lastState = getStatesInString(s1);
                var curState = getStatesInString(s2);
                if (s1 != s2) {
                    curState = getStateChange(curState, lastState);

                    if (curState[0] != "void")
                        element.transition().text(curState.join(" ; ")).attr("fill", color).duration(speed).delay(time + wait);

                }
            }

            // get groups elements
            element = svg.selectAll("[id='" + svgIDs[i] + "_" + s2.replace("'", "&#39;") + "']");
            // alert("[id='" + svgIDs[i] + "_" + s2.replace("'", "&#39;") + "']");
            if (!checkUndefined(element) && element[0].length > 0) {
                element.attr("class", "groups show");
                // alert("checked: " + "[id='" + svgIDs[i] + "_" + s2.replace("'", "&#39;") + "']");
            }
        }

        // reduntdant code
        /*
        var classElement = document.getElementsByClassName(s2);
        if (classElement) {
            // var classElementColor = element.attr("fill");

            for (var k = 0; k < classElement.length; k++) {
                
                classElement[k].classList.remove("hidden");
                classElement[k].classList.add("show");
                //  classElement[k].setAttribute("fill", classElementColor);

            }
        }
        */
        /*
        // display items which have mapped values 
        var element = svg.selectAll("[id='" + s2.replace("'", "&#39;") + "']");
        // alert("[id='" + s2.replace("'", "&#39;") + "']" );
        if (!checkUndefined(element) && element[0].length > 0) {
            element.attr("class", "groups show")
        }
        */

        //s2_class.classList.add("show");

    }
    
    // display condition cases
    if (!checkUndefined(conditionDisplay) &&  Object.getOwnPropertyNames(conditionDisplay).length > 1){
        let keys = conditionDisplay.keys;
        let values = conditionDisplay.values;
        for (let i = 0; i < keys.length; ++i){
            element = svg.selectAll("[id='" + keys[i] + "']");
            let res = "";
            let Y = values[i][keys[i]];
            for (let j = 0; j < Y.length; ++j){
                res = Object.keys(Y[j])[0];                 // Y[i] is always 1 element
                let condition = Object.values(Y[j]);        // array of condition
                if (res.split('****').length > 1){          // in case result in a function parameter as priv(queue,_) or priv(_,ln)
                    break;
                }
                else{
                    let checkCondition = true;
                    condition = condition[0];
                    for (let k = 0; k < condition.length; ++k){
                        let c = condition[k].split('==');
                        if (c.length > 1){ // true
                            let keyInState = c[0].trim();
                            let valueInState = c[1].trim();
                            //console.log(next_state[])
                            s2 = next_state[keyInState].trim();
                            
                            if (s2 !== valueInState){
                                checkCondition = false;
                                break;
                            }
                        }
                    }
                    if (checkCondition === true){
                        break;
                    }

                }
            }
            element.transition().text(next_state[res]).attr("fill", "black").duration(speed).delay(time+wait);
        }
    }

    // display queueVisualDisplay
    if (!checkUndefined(queueVisualDisplay) &&  Object.getOwnPropertyNames(queueVisualDisplay).length > 1){
        // example
        // queueVisualDisplay['lane[0]'] = array
        // queueVisualDisplay['lane[0]'][0] -> "left"
        // queueVisualDisplay['lane[0]'][1] -> ";"
        // queueVisualDisplay['lane[0]'][2] -> "v[_]#2"
        let removeSpaceInQueue = function(q, splitSymbol){ // return array of element of queue without space inside
            let t = q.split(splitSymbol); // array of element
            for (let i = 0; i < t.length; i++){
                t[i] = t[i].trim();
            }
            return t;
        }
        
        
        for (const key of Object.keys(queueVisualDisplay)){ //key: lane[0]
            // result is array of element in queue
            let valueQueueInput = removeSpaceInQueue(next_state[key].trim(), queueVisualDisplay[key][1]);
            // check order of visualdisplay (start from left or right)
            if (queueVisualDisplay[key][0] == "left"){
                for (let i = 0; i < valueQueueInput.length; i++){
                    // result is v[0]#2 or v[0]
                    let valueForDisplay = queueVisualDisplay[key][2].replace('_', valueQueueInput[i]);

                    // check the composite data or not
                    let isCompositeData = valueForDisplay.split('#');
                    // incase v[0]
                    if (isCompositeData.length == 1){
                        // search svg with id = svgid_value_(i+1)
                        element = svg.selectAll("[id='" + valueForDisplay + "_" + next_state[valueForDisplay].trim().replace("'", "&#39;") + i + "']");
                        if (!checkUndefined(element) && element[0].length > 0) {
                            element.attr("class", "groups show");
                        }
                    }
                    // in case v[0]#2
                    else{
                        try {
                            // in case cannot read data of next_state[isCompositeData[0]]
                            // or queue contain empty/oo/... element
                            if (!next_state.hasOwnProperty(isCompositeData[0])){
                                break;
                            }
                            tmp = Parser(next_state[isCompositeData[0]]);
                            
                            // convert text number into array
                            let getValueCondition = function (sample) {
                                let arr = [];
                                for (let j = 1; j < isCompositeData.length; j++) {
                                    arr.push(parseInt(isCompositeData[j]));
                                }
                                return arr;
                            }
                            
                            var tmpV = Compare(tmp, getValueCondition(isCompositeData));
                            console.log("isCompositeData: ");
                            console.log(valueForDisplay);
                            // search svg with id = svgid_value_(i+1)
                            element = svg.selectAll("[id='" + valueForDisplay + "_" + tmpV.replace("'", "&#39;") + "_" + i + "']");
                            if (!checkUndefined(element) && element[0].length > 0) {
                                element.attr("class", "groups show");
                            }
                        } catch (error) {
                            alert("cannot parse composite data, please check input file, data - " + isCompositeData[0]);
                            return;
                        }
                        
                        
                    }
                }
            }
            else if (queueVisualDisplay[key][0] == "right"){

            }



        }
    }
    $("#diplayStateText").html("State " + (index) + " :" + displayState);

    //    elementDisplayState.transition().text("State " + (index) + " :"
    //                    + displayState).attr("fill", "black").attr("stepIndex", index.toString()).duration(speed).delay(time);
    //

}

// paramaters: 2 states s1, s2
// return: states in s1 which not in s2. type of array
function getStateChange(s1, s2) {
    var lens1 = s1.length;
    var lens2 = s2.length;
    var temp = [];
    for (var i = 0; i < lens1; ++i) {
        var flag = false;
        for (var j = 0; j < lens2; ++j) {
            if (s1[i] == s2[j]) {
                flag = true;
                break;
            }
        }
        if (flag == false) {
            temp.push(s1[i]);
        }
    }
    return temp;
}

function getSameState(s1, s2) {
    var lens1 = s1.length;
    var lens2 = s2.length;
    var dics1 = {};
    var dics2 = {};
    var temp = [];
    for (var i = 0; i < lens1; ++i) {
        if (!(s1[i] in dics1)) {
            dics1[s1[i]] = 0;
        }
        else {
            dics1[s1[i]]++;
        }
    }
    for (var i = 0; i < lens2; ++i) {
        if (!(s2[i] in dics2)) {
            dics2[s2[i]] = 0;
        }
        else {
            dics2[s2[i]]++;
        }
    }
    for (var key in dics1) {
        if (key in dics2) {
            if (dics1[key] == 0) {
                continue;
            }
            else if (dics2[key] == 0) {
                continue;
            }
            else {
                temp.push(key);
                dics1[key]--;
                dics2[key]--;
            }
        }
    }
    return temp;
}
function getStatesInString(str) { // paramater is string
    var s = str.split(";");
    for (var i = 0; i < s.length; ++i) {
        s[i] = s[i].trim();
    }
    return s;
}


function isEqualState(s1, s2) {
    var l1 = s1.length;
    var l2 = s2.length;
    var count = 0;
    for (var i = 0; i < l1; ++i) {
        for (var j = 0; j < l2; ++j) {
            if (s1[i] == s2[j]) {
                count++;
                continue;
            }
        }
    }
    if (count == l1) {
        return true;
    }
    else return false;
}

// get value from position nw_msg#0 of key svgIDs[i] in state s1
function getMessValue(state,key,possition){
    let tmp = possition; // nw_msg#0 or nw_#0#1
    let n = tmp.length;
    let result = "";
    let iter = key.length + 1; 
    let check = false;
    let iterState = 0;
    let tmpState = state.trim();
    let nState = tmpState.length;
    let nPos = 0;
    // check nw_msg#0
    for (let i = key.length + 1; i < n; ++i){
        if (tmp[i] === tmpState[iterState]){
            iter = i;
            check = true;
            iterState++;
        }
        else{
            if (check === true){
                break;
            }
            if (tmp[i] === '#'){
                break;
            }
        }
    }
    // check = true ==> exist msg
    // check = false ==> not exist msg
    if (check === true){
        nState--; // delete the last ')'
        iterState++;
    }
    //       iter                          iter
    //       |                             |
    // nw_msg#0            or           nw_#0#1
    //     iterstate                        iterstate
    //     |                                |
    // msg(...)                             (abc,
    
    for (let i = iter; i < n; ++i){
        let numComma = 0;
        if (tmp[i] === '#'){
            continue;
        }
        else{
            // number and inside
            if (i + 1 < n && tmp[i+1] === '#'){ 
                // only move to 
                while (iterState < nState && numComma < parseInt(tmp[i],10)){
                    if (isOpenLetter(tmpState[iterState])){
                        stack.push(tmpState[iterState]);
                        iterState++;
                    }
                    if (isCloseLetter(tmpState[iterState])){
                        stack.pop();
                        iterState++;
                    }
                    if (isCommaLetter(tmpState[iterState]) && stack.isEmpty()){
                        numComma++;
                        iterState++;
                    }
                }
            }
            else { 
                // only number
                // move to possition tmp[i]
                while (iterState < nState && numComma < parseInt(tmp[i],10)){
                    if (isOpenLetter(tmpState[iterState])){
                        stack.push(tmpState[iterState]);
                        iterState++;
                    }
                    if (isCloseLetter(tmpState[iterState])){
                        stack.pop();
                        iterState++;
                    }
                    if (isCommaLetter(tmpState[iterState])){
                        numComma++;
                        iterState++;
                    }
                }
                while (iterState < nState){
                    if (isCommaLetter(tmpState[iterState])){
                        break;
                    }
                    else{
                        result = result + tmpState[iterState];
                        iterState++;
                    }
                    return result;
                }


            }

        }
    }
}



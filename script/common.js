
function drawText(svg, txt, x, y, color, size, id) {
    var text = svg.append("text")
        .text(txt)
        .attr("text-anchor", "middle")
        .attr("x", x)
        .attr("y", y)
        .attr("id", id)
        .attr("font-family", "sans-serif")
        .attr("font-size", size + "px")
        .attr("fill", color);
    return text;
}

function drawRect(svg, x, y, w, h, color, id) {
    var rect = svg.append("rect")
        .attr("x", x)
        .attr("y", y)
        .attr("width", w)
        .attr("height", h)
        .attr("id", id)
        .attr("fill", color);
    return rect;
}
function drawLine(svg, x1, y1, x2, y2, color, id) {
    svg.append("line")
        .attr("x1", x1)
        .attr("y1", y1)
        .attr("x2", x2)
        .attr("y2", y2)
        .attr("id", id)
        .attr("stroke", color);
}


function reset() {
    initTime();
    allData = {};
    lengthData = 0;
    lengthLoopStates = 0;
    stepIndex = 0;
    stepLoopIndex = 0;

    $("#statePatterns").html("");
    svgSPIndex = 0;

    allStatePatterns = [];
    regexPatterns = [];
    d3.select("#diplayStateText").text("");

}

function abortRead() {
    reader.abort();
}

function errorHandler(evt) {
    switch (evt.target.error.code) {
        case evt.target.error.NOT_FOUND_ERR:
            alert('File Not Found!');
            break;
        case evt.target.error.NOT_READABLE_ERR:
            alert('File is not readable');
            break;
        case evt.target.error.ABORT_ERR:
            break; // noop
        default:
            alert('An error occurred reading this file.');
    };
}

function updateProgress(evt) {
    // evt is an ProgressEvent.
    if (evt.lengthComputable) {
        var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
        // Increase the progress bar length.
        if (percentLoaded < 100) {
            progress.style.width = percentLoaded + '%';
            progress.textContent = percentLoaded + '%';
        }
    }
}

function loadFile() {
    document.getElementById('fileForUpload').value = "";
}

function handleFileSelect(evt) {

    // Reset progress indicator on new file selection.
    progress.style.width = '0%';
    progress.textContent = '0%';

    reader = new FileReader();
    reader.onerror = errorHandler;
    reader.onprogress = updateProgress;
    reader.onabort = function (e) {
        alert('File read cancelled');
    };
    reader.onloadstart = function (e) {
        document.getElementById('progress_bar').className = 'loading';
    };
    var files = document.getElementById('fileForUpload').files;
    if (!files.length) {
        alert('Please select a file!');
        return;
    }
    var file = files[0];
    var start = 0;
    var stop = file.size - 1;
    reader.onload = function (evt) {
        // Ensure that the progress bar displays 100% at the end.
        progress.style.width = '100%';
        progress.textContent = '100%';
        setTimeout("document.getElementById('progress_bar').className='';  $('.run').attr('disabled', false);", 800);

        if (evt.target.readyState == FileReader.DONE) { // DONE == 2
            contents = evt.target.result;
            allKeys = contents.split('###textDisplay')[0].split('###keys')[1].trim().split(' ');

            // conditionDisplay
            let conDisplayData = contents.split('###conditionDisplay');
            
            if (conDisplayData.length > 1) {
                conDisplayData = conDisplayData[1].trim().split('###states')[0].trim().split('\n');
                //conditionDisplay = {};
                conditionDisplay.keys = [];
                conditionDisplay.values = [];
                // structure of conditionDisplay 
                // keys = [] --> [ln, queue]
                // values = [] -->  [X1, X2]
                // X = {} --> {ln: Y1, queue: Y2}
                // Y = [] --> [Z1, Z2]
                // Z = {} --> {ln1 : T}
                // T = [] --> [havePriv[1]==true, ...]

                conditionDisplay.keys = conDisplayData[0].trim().split(' '); // ln queue ==> array
                let keys = conditionDisplay.keys;
                let iter = 1; // conDisplayData
                for (let i = 0; i < keys.length; ++i) {

                    let line = conDisplayData[iter].trim().split(' '); // ln[1] ln[2] ln[3] ****priv(_,ln) ==> array
                    let Y = [];

                    iter++;
                    for (let j = iter; j < line.length + iter; ++j) {
                        let T = conDisplayData[j].trim().split('++++');  // ln[1] havePriv[1]==false ...
                        let temp = T.shift();                                   // ln[1]
                        let Z = {};
                        Z[temp] = T;                                            // ln[1] : [havePri[1]==false, ...]
                        Y.push(Z);
                    }
                    iter = iter + line.length;
                    // Z = {ln[1]: ..., ln[2]: ...}
                    let X = {};
                    X[keys[i]] = Y;
                    conditionDisplay.values.push(X);
                }
                console.log(conditionDisplay);
            }

            // messDisplay
            let messDisplayData = contents.split('###messDisplay');
            if (messDisplayData.length > 1){
                messDisplayData = messDisplayData[1].trim().split('###states')[0].trim().split('\n');
                messDisplay.messContainerName = []; // nw network
                messDisplay.messContainer = {}; // [nw]: msg#0#1 msg#1 [network]: msg
                // structure of message
                // nw network
                // msg#0#1 msg#1
                // msg  
                messDisplay.messContainerName = messDisplayData[0].trim().split(' '); // nw network
                let n = messDisplay.messContainerName.length;
                for (let i = 0; i < n; ++i){
                    messDisplay.messContainer[messDisplay.messContainerName[i-1]] = messDisplayData[i+1].trim().split(' ');
                }
            }
            //textDisplay
            var displayInput = contents.split('###textDisplay')[1].split('###states')[0].trim().split('\n');
            // console.log("displayInput: " + displayInput);
            var lenOfDisplayInput = displayInput.length;
            // alert("len of display input: " + lenOfDisplayInput);

            // take texDisplays
            for (var i = 0; i < lenOfDisplayInput; i++) {
                var element = displayInput[i].split('::::');
                var value = [];
                // check display for queue (with visual)
                // other will be implemented later
                if (element.length >= 5){
                    // example:
                    // element[0]: "lane[0]" (name of abstract prototype)
                    // element[1]: "QUEUE" (kind of abstract prototype)
                    // element[2]: "left" (order starts from left (or right))
                    // element[3]: ";" (separate by ;)
                    // element[4]: "visualdisplay" (type of display)
                    // element[5]: "v[_]#2" (in case visualdisplay, what data links to )
                    if (element[1] == "QUEUE"){
                        if (element[4] == "visualdisplay"){
                            queueVisualDisplay[element[0]] = [];
                            queueVisualDisplay[element[0]].push(element[2]);
                            queueVisualDisplay[element[0]].push(element[3]);
                            queueVisualDisplay[element[0]].push(element[5]);
                        }
                    }
                }
                else {
                    if (!checkUndefined(element[1])) {
                        if (element[1] == "EXT") {
                            i++;
                            value.push(displayInput[i].split(' '));
                            texDisplays[element[0]] = value;
                            // element[0] = key
                            // element[1] = extract
                            // element[2] = #1, #2
                        }
                        else {
                            value.push(element[1]);
                            value.push(element[2].split('++++'));
                            //alert("element[2].split('++++'): " + element[2].split('++++'));

                            texDisplays[element[0]] = value; // texDisplays is a global variable.
                            //element[0] key
                            //element[1] option
                            //element[2] <_,_>, empty
                        }
                    }
                }
            }
            //console.log("texDisplay(not update): " + Object.keys(texDisplays).length);
            // remove all break lines
            contents = contents.replace(/(\r\n|\n|\r)/gm, "").replace(/\s+/g, " ");
            contents = contents.replace(/\s+/g, " ").replace(/\s+/g, " ");
        }
    }

    var blob = file.slice(start, stop + 1);
    //alert("blob: " + typeof(blob));
    reader.readAsBinaryString(blob);

    reset();
    // Read in the image file as a binary string.
    // reader.readAsBinaryString(evt.target.files[0]);
}

// end script

function formatString(value) {

    if (value.endsWith(" (")) {
        value = value.substring(0, value.length - 2);
    }
    var equal = 0;
    if (value.endsWith(")")) {
        
        if (value.split("(").length == value.split(")").length) {
            equal = 1;
            if (value.startsWith("(") && Object.keys(texDisplays).length == 0) {
                value = value.substring(1, value.length - 1);
            }
        }
        else 
            value = value.substring(0, value.length - 1);
    }
    return value;
}




function getItems(ele, text) {

    var l = ele.length;
    var item = ele[0];
    for (var i = 1; i < l; i++) {
        item = item + "|" + ele[i];
    }
    item = item.replace(/_/g, '.*?');

    var regex = new RegExp(item, "g");

    var result = text.match(regex);
    if (!checkUndefined(result)) {
        for (var i = 0; i < result.length; i++) {
            result[i] = result[i].trim();
        }
    }
    return result;
}


function NormalizeText(key, text) {
    var tmpValue = texDisplays[key];
    if (checkUndefined(tmpValue)) return text;
    else {
        var option = tmpValue[0];
        var result = getItems(tmpValue[1], text);
        console.log("tmpValue: ");
        console.log(tmpValue);
        console.log("result: ");
        console.log(result);
        if (!checkUndefined(result)) {
            var t = result.length;
            if (option == "REV" || option == "VER-REV") {
                result = result.reverse();
            }
            if (option == "REV" || option == "VER-REV" || option == "NONE") {
                var newText = result[0];
                for (var i = 1; i < t; i++) {
                    newText = newText + " " + result[i];
                }
                return newText;

            }
        }

        return " ";
    }
}

function NormalizeTextDisplay(key, text, color, dur, delay) {
    var tmpValue = texDisplays[key];
    if (!checkUndefined(tmpValue)) {
        var option = tmpValue[0];
        var result = getItems(tmpValue[1], text);
        if (!checkUndefined(result)) {
            var t = result.length;
            if (option == "VER-REV") {
                result = result.reverse();
                var newText = result[0];
                for (var i = 1; i < t; i++) {
                    newText = newText + " " + result[i];
                }
                return newText;
            }
            if (option == "VER" || option == "VER-REV") {
                var svg1 = svg.selectAll("[id='" + key.replace("'", "&#39;") + "']");

                var y = svg1.attr("y");
                //svg1.attr("y", y);
                var x = svg1.attr("x");

                var lineHeight = 1.3;

                // svg1text(null).duration(dur).delay(delay);

                var tspan = svg1.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", lineHeight + "em");

                for (var i = 0; i < result.length; i++) {

                    var word = result[i];

                    tspan.text(word).attr("fill", color);

                    lineHeight = (lineHeight - 1 * 1.0);

                    tspan = svg1.append("tspan").attr("x", x).attr("y", y).attr("dy", lineHeight + "em");

                }

            }
        }

    }
}
//old function
function loadDataSet(lines, keys) {
    var numOfLines = lines.length;
    var dataset = [];
    var numOfKeys = keys.length;
    for (var i = 0; i < numOfLines; i++) {
        if ((i == numOfLines - 1) && lines[i].trim() == "nil") {
            // do nothing.
        }
        else {

            var trimedLine = lines[i].trim();

            var line = trimedLine.substring(1, trimedLine.length - 1) || ""; // each state include "()" except last state: (..) || (..) || ..

            // in case last line
            if ((i == numOfLines - 1) && checkUndefined(lines[i]) == false) {
                var str = trimedLine;
                if (str[0] != "(") line = trimedLine || "";
            }
            if (i == numOfLines - 1) line = trimedLine || "";
            var state = {};
            for (var j = 0; j < numOfKeys; j++) {
                //remove cac space thanh 1 space
                var tmp = (line.replace(/\s+/g, " ").split(keys[j] + ":"))[1];

                if (checkUndefined(tmp)) {
                    tmp = (line.replace(/\s+/g, " ").split(keys[j] + " :"))[1];
                }
                var value;
                if ((j + 1) == numOfKeys) { // last substate

                    value = tmp.trim();
                }
                else {
                    var arrValue = tmp.split(keys[(j + 1)] + ":");
                    if (arrValue && arrValue.length > 1) {
                        value = arrValue[0].trim();
                    }
                    else {
                        arrValue = tmp.split(keys[(j + 1)] + " :");
                        value = arrValue[0].trim();
                    }

                }
                // console.log("value: " + value);
                value = formatString(value);
                // console.log("value (update): " + value);
                state[keys[j]] = value;

            }
            dataset.push(state);
        }

    }

    return dataset;

}

//new function
function loadDataSet2(lines, keys) {
    var numOfLines = lines.length;
    var dataset = [];
    var numOfKeys = keys.length;

    for (var i = 0; i < numOfLines; i++) {
        var trimedLine = lines[i].trim();
        if ((i == numOfLines - 1) && trimedLine == "nil") {
            // do nothing.
        }
        else {
            if (trimedLine) {
                var str = trimedLine;
                for (var j = 0; j < numOfKeys; j++) {
                    var indexKey = str.indexOf(keys[j]);
                    var indexValue = str.indexOf(":");

                }
            }
        }
    }

    return dataset;
}

function loadData() {
    allData = {};
    loopStates = {};
    lengthData = 0;
    lengthLoopStates = 0;



    try {
        /*** The input structure:
             ###keys
             ###textDisplays
         
             ###statessvgIDs
             ###loop
        ***/

        // keys
        var keys = contents.split('###keys')[1].split('###textDisplay')[0].trim().split(" ");

        svgIDs = keys;
        allKeys = keys;

        //states
        var states = contents.split('###states')[1].split('###loop')[0];

        //loop
        var loop = contents.split('###loop')[1];
        if (!checkUndefined(loop)) {
            var loopArray = loop.trim().split('||');
            loopStates = loadDataSet(loopArray, keys); // loopStates is a global variable.
            lengthLoopStates = loopStates.length;
        }

        var lines = states.trim().split('||');


        var dataset = loadDataSet(lines, keys);
        lengthData = dataset.length;

        return dataset;

    }
    catch (err) {
        alert(err.message);
    }

}


function loadRules() {
    var dataset = {};
    var input = contents.split('###rules');
    if (input.length > 1) {
        var rules = input[1].split('###keys')[0];
        var lines = rules.split('\n');
        for (var i = 0; i < lines.length; i++) {
            var ruleLine = lines[i].split(':');
            if (ruleLine.length > 0) {
                var key = ruleLine[0].replace(/\s/g, '');;
                if (checkUndefined(key) == false && key != "") {
                    var value = ruleLine[1].split('|');
                    var term = []; var state = {}
                    for (var m = 0; m < value.length; m++) {
                        var data = value[m].split(' ');
                        var key2 = data[0]; var value2 = data[1];
                        if (checkUndefined(key2) == false && checkUndefined(value2) == false)
                            state[key2] = value2;
                    }
                    term.push(state);
                    dataset[key] = state;
                }
            }
        }
    }

    return dataset;
}

function checkUndefined(variable) {
    if (variable === undefined || variable === null)
        return true;
    else
        return false;
}

function hasSameProps(obj1, obj2) {
    var obj1Props = Object.keys(obj1),
        obj2Props = Object.keys(obj2);
    return obj1Props.every(function (prop) {
        var index = obj2Props.indexOf(prop);
        return ((index >= 0) && (obj1[prop] == obj2[prop]));
    });

}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}




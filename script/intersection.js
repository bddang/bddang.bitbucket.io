
function displayConcurrentConflictLane(){
    //console.log("here");
    let temp = d3.selectAll("svg > *");
    temp.each(function(){
        let id = d3.select(this).attr('id');
        //console.log(id);
        let getType = id.split('_');
        // getType[0]: name (lane0, lane1, etc.)
        // getType[1]: condition type (intersection)
        // getType[2]: type of law (left, right)
        if (getType[1] == "intersection"){
            if (getType[2] == "left"){

            }
            else if (getType[2] == "right"){
                let id = getType[0] + "_" + getType[1] + "_" + getType[2];
                let te = d3.selectAll("[id='" + id + "']");
                
                te.on("click", function(){
        
                    let laneNumber = parseInt(getType[0][getType[0].length-1],10);
                    // check if click twice then reset color.
                    let color1 = d3.selectAll("[id='" + id + "']").style("fill");
                    let nextLaneID = "lane" + ((laneNumber + 1) % 8).toString() + "_" + getType[1] + "_" + getType[2];
                    let color2 = d3.selectAll("[id='" + nextLaneID + "']").style("fill");

                    if (color1 != color2 && color1 == "rgb(0, 0, 0)"){
                        for (let i = 0; i < 8; i++){
                            let resetLane = "lane" + i.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + resetLane + "']").style("fill", "black");
                        }
                    }

                    // coloring concurrent and conflict lane.
                    else{
                        if (laneNumber % 2 == 0){
                            // display red color for conflict lane
                            let conflictLaneNumber = (laneNumber + 2) % 8;
                            let name = "lane" + conflictLaneNumber.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "red");

                            conflictLaneNumber = (laneNumber + 5) % 8;
                            name = "lane" + conflictLaneNumber.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "red");
                            
                            conflictLaneNumber = (laneNumber + 6) % 8;
                            name = "lane" + conflictLaneNumber.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "red");
                            
                            conflictLaneNumber = (laneNumber + 7) % 8;
                            name = "lane" + conflictLaneNumber.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "red");

                            // ---------------------- //
                            // display blue color for concurrent lane
                            let concurrentLane = (laneNumber + 1) % 8;
                            name = "lane" + concurrentLane.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "blue");

                            concurrentLane = (laneNumber + 3) % 8;
                            name = "lane" + concurrentLane.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "blue");

                            concurrentLane = (laneNumber + 4) % 8;
                            name = "lane" + concurrentLane.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "blue");
                        }
                        else{
                            let conflictLaneNumber = (laneNumber + 1) % 8;
                            let name = "lane" + conflictLaneNumber.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "red");

                            conflictLaneNumber = (laneNumber + 2) % 8;
                            name = "lane" + conflictLaneNumber.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "red");
                            
                            conflictLaneNumber = (laneNumber + 3) % 8;
                            name = "lane" + conflictLaneNumber.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "red");
                            
                            conflictLaneNumber = (laneNumber + 6) % 8;
                            name = "lane" + conflictLaneNumber.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "red");

                            // ---------------------- //
                            // display blue color for concurrent lane
                            let concurrentLane = (laneNumber + 4) % 8;
                            name = "lane" + concurrentLane.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "blue");

                            concurrentLane = (laneNumber + 5) % 8;
                            name = "lane" + concurrentLane.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "blue");

                            concurrentLane = (laneNumber + 7) % 8;
                            name = "lane" + concurrentLane.toString() + "_" + getType[1] + "_" + getType[2];
                            d3.selectAll("[id='" + name + "']").style("fill", "blue");
                        }
                        d3.selectAll("[id='" + id + "']").style("fill", "black");
                    }
                });
            }
        }
    }
    );

}

